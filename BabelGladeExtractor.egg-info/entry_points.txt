
    [babel.extractors]
    glade = babelglade.extract:extract_glade
    desktop = babelglade.extract:extract_desktop

    [distutils.commands]
    compile_catalog = babel.messages.frontend:compile_catalog
    
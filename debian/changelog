python-babelgladeextractor (0.7.0-3) unstable; urgency=medium

  * Enable building twice in a row.
    Closes: #1048870
  * Add upstream metadata.
  * Bump Standards-Version.
  * Bump d/copyright date for debian directory.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 15 Dec 2023 18:15:19 +0100

python-babelgladeextractor (0.7.0-2) unstable; urgency=medium

  * Mark autopkgtest as superficial.
    Closes: #971496
  * Bump Standards-Version.
  * Add Rules-Requires-Root.
  * Use debhelper-compat 13.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 07 Oct 2020 20:22:03 +0200

python-babelgladeextractor (0.7.0-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Mon, 20 Jan 2020 06:45:50 +0100

python-babelgladeextractor (0.6.3-1) unstable; urgency=medium

  * New upstream release.
  * Re-enable tests.

 -- Sascha Steinbiss <satta@debian.org>  Thu, 19 Dec 2019 13:00:55 +0100

python-babelgladeextractor (0.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Remove dependency on python3-lxml.
  * Remove patches applied upstream.

 -- Sascha Steinbiss <satta@debian.org>  Mon, 25 Nov 2019 13:43:13 +0100

python-babelgladeextractor (0.5.1-3) unstable; urgency=medium

  * Add patch to provide pofiles in order.
    This is required to obtain deterministic .desktop and .appdata.xml
    files, which makes builds of packages depending on this reproducible.

 -- Sascha Steinbiss <satta@debian.org>  Sun, 24 Nov 2019 11:59:18 +0100

python-babelgladeextractor (0.5.1-2) unstable; urgency=medium

  * Add autopkgtest.
  * Source upload for testing migration.

 -- Sascha Steinbiss <satta@debian.org>  Thu, 14 Nov 2019 16:23:26 +0100

python-babelgladeextractor (0.5.1-1) unstable; urgency=medium

  * Initial release. (Closes: #944328)

 -- Sascha Steinbiss <satta@debian.org>  Thu, 07 Nov 2019 23:49:33 +0100
